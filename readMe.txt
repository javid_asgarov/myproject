README

Project designed to practice skills of working with collections. A number of times 
each character is present in an input message is calculated, a map with results is outputted. 
Results are also cached for optimized performance.
