package com.asgarov.character_counter;

import java.util.Map;

public class CharacterPrinter {
    public String print(String message, Map<Character, Long> uniqueCharacters) {
        StringBuilder result = new StringBuilder(message).append("\n");
        uniqueCharacters.forEach((character, number) -> {
            result.append("\"" + character + "\"" + " - " + number + "\n");
        });
        return result.toString();
    }
}
