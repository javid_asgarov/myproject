package com.asgarov.character_counter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CharacterCounter {
    private Map<String, Map<Character, Long>> cache = new HashMap<>();

    public Map<Character, Long> countUniqueCharacters(String message) {
        validateMessage(message);

        Map<Character, Long> uniqueCharacters = cache.get(message);
        if (uniqueCharacters == null) {
            uniqueCharacters = fillTheMapOfUniqueCharacters(message);
            cache.put(message, uniqueCharacters);
        }

        return uniqueCharacters;
    }

    private void validateMessage(String message) {
        String exceptionMessage;
        if (message == null) {
            exceptionMessage = "Input can't be null";
        } else if (message.isEmpty()) {
            exceptionMessage = "Input can't be empty";
        } else {
            return;
        }
        throw new IllegalArgumentException(exceptionMessage);
    }

    private Map<Character, Long> fillTheMapOfUniqueCharacters(String message) {
        return message.chars().mapToObj(character -> (char) character)
                .collect(Collectors.groupingBy(character -> character, LinkedHashMap::new, Collectors.counting()));
    }
}
