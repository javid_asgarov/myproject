package com.asgarov.character_counter;

import org.junit.jupiter.api.Test;

import com.asgarov.character_counter.CharacterCounter;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CharacterCounterTest {
    @Test
    void processShouldThrowExceptionWhenStringIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> new CharacterCounter().countUniqueCharacters(""));
    }

    @Test
    void processShouldThrowExceptionWhenStringIsNull() {
        assertThrows(IllegalArgumentException.class, () -> new CharacterCounter().countUniqueCharacters(null));
    }

    @Test
    void processShouldPerformNormallyWhenInputIsOneLetter() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("h");
        long expectedAmount = 1;
        long actualAmount = map.get('h');
        assertEquals(expectedAmount, actualAmount);

        int expectedSize = 1;
        assertEquals(expectedSize, map.size());
    }

    @Test
    void processShouldPerformNormallyWhenInputIsOneWord() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("hello");

        long expectedNumberOfH = 1;
        assertEquals(expectedNumberOfH, map.get('h'));
        long expectedNumberOfE = 1;
        assertEquals(expectedNumberOfE, map.get('e'));
        long expectedNumberOfL = 2;
        assertEquals(expectedNumberOfL, map.get('l'));
        long expectedNumberOfO = 1;
        assertEquals(expectedNumberOfO, map.get('o'));

        int expectedSize = 4;
        assertEquals(expectedSize, map.size());

    }

    @Test
    void processShouldPerformNormallyWhenInputIsSeveralWords() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("hello world!");

        assertEquals(1, map.get('h'));
        assertEquals(1, map.get('e'));
        assertEquals(3, map.get('l'));
        assertEquals(2, map.get('o'));
        assertEquals(1, map.get('w'));
        assertEquals(1, map.get('r'));
        assertEquals(1, map.get('d'));
        assertEquals(1, map.get('!'));
        assertEquals(1, map.get(' '));

        int expectedSize = 9;
        assertEquals(expectedSize, map.size());
    }

    @Test
    void processShouldPerformNormallyWhenInputHasOnlyDigits() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("123114567");

        assertEquals(3, map.get('1'));
        assertEquals(1, map.get('2'));
        assertEquals(1, map.get('3'));
        assertEquals(1, map.get('4'));
        assertEquals(1, map.get('5'));
        assertEquals(1, map.get('6'));
        assertEquals(1, map.get('7'));

        int expectedSize = 7;
        assertEquals(expectedSize, map.size());
    }

    @Test
    void processShouldPerformNormallyWhenInputHasOnlySigns() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("!@#!!$%");

        assertEquals(3, map.get('!'));
        assertEquals(1, map.get('@'));
        assertEquals(1, map.get('#'));
        assertEquals(1, map.get('$'));
        assertEquals(1, map.get('%'));

        int expectedSize = 5;
        assertEquals(expectedSize, map.size());
    }

    @Test
    void processShouldPerformNormallyWhenInputHasBothLettersAndSigns() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("abc!@#!!$%aa");

        assertEquals(3, map.get('!'));
        assertEquals(1, map.get('@'));
        assertEquals(1, map.get('#'));
        assertEquals(1, map.get('$'));
        assertEquals(1, map.get('%'));
        assertEquals(3, map.get('a'));
        assertEquals(1, map.get('b'));
        assertEquals(1, map.get('c'));

        int expectedSize = 8;
        assertEquals(expectedSize, map.size());
    }

    @Test
    void processShouldPerformNormallyWhenInputHasBothLettersAndDigits() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("abc123331aa");

        assertEquals(2, map.get('1'));
        assertEquals(1, map.get('2'));
        assertEquals(3, map.get('3'));
        assertEquals(3, map.get('a'));
        assertEquals(1, map.get('b'));
        assertEquals(1, map.get('c'));

        int expectedSize = 6;
        assertEquals(expectedSize, map.size());
    }

    @Test
    void processShouldPerformNormallyWhenInputHasLettersSignsAndDigits() {
        Map<Character, Long> map = new CharacterCounter().countUniqueCharacters("abc1233!!#@31aa");

        assertEquals(3, map.get('a'));
        assertEquals(1, map.get('b'));
        assertEquals(1, map.get('c'));
        assertEquals(2, map.get('1'));
        assertEquals(1, map.get('2'));
        assertEquals(3, map.get('3'));
        assertEquals(2, map.get('!'));
        assertEquals(1, map.get('#'));
        assertEquals(1, map.get('@'));

        int expectedSize = 9;
        assertEquals(expectedSize, map.size());
    }
    
    @Test
    void processShouldReturnACorrectlyFormattedString() {
        String message = "Hello World!";
        Map<Character, Long> uniqueCharacters = new CharacterCounter().countUniqueCharacters(message);

        String expected = "Hello World!\n" +
                          "\"H\" - 1\n" +
                          "\"e\" - 1\n" +
                          "\"l\" - 3\n" +
                          "\"o\" - 2\n" +
                          "\" \" - 1\n" +
                          "\"W\" - 1\n" +
                          "\"r\" - 1\n" +
                          "\"d\" - 1\n" +
                          "\"!\" - 1\n";
        String actual = new CharacterPrinter().print(message, uniqueCharacters);
        assertEquals(expected, actual);
    }

    @Test
    void processShouldReturnACorrectlyFormattedStringWhenInputIsOneWord() {
        String message = "hello";
        Map<Character, Long> uniqueCharacters = new CharacterCounter().countUniqueCharacters(message);

        String expected = "hello\n" +
                          "\"h\" - 1\n" +
                          "\"e\" - 1\n" +
                          "\"l\" - 2\n" +
                          "\"o\" - 1\n";

        String actual = new CharacterPrinter().print(message, uniqueCharacters);
        assertEquals(expected, actual);
    }
}
